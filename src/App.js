import "./App.scss";
import { Container } from "react-bootstrap";
import NavigationBar from "./components/navigation-bar/NavigationBar";
import Carousel from "./components/carousel/Carousel";
import Content from "./components/content/Content";
import ContactContainer from "./components/contact-container/ContactContainer";
import Footer from "./components/footer/Footer";

function App() {
  return (
    <Container fluid>
      <NavigationBar />
      <Carousel />
      <Content />
      <ContactContainer />
      <Footer />
    </Container>
  );
}

export default App;
