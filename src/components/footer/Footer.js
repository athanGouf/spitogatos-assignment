/**
 * Footer of page
 * Cookies and privacy are not visible on small devices max 768px
 */
import { Col, Button } from "react-bootstrap";
import logo from "../../assets/images/logo.svg";
import "./styles.scss";

const Footer = () => (
  <div className="cont row">
    <Col sm lg="4" className="d-flex align-items-center copyright">
      © The standard Copywrite 2020
    </Col>
    <Col
      lg="4"
      className="d-lg-flex justify-content-center align-items-sm-center display-sm-none"
    >
      <Button className="btn btn-dark language">Cookies.</Button>
      <div className="verticalLine" />
      <Button className="btn btn-dark language">Privacy.</Button>
    </Col>
    <Col
      sm
      lg="4"
      className="d-flex justify-content-end align-items-center pr-5 logo-container"
    >
      <img src={logo} alt="logo" />
    </Col>
  </div>
);

export default Footer;
