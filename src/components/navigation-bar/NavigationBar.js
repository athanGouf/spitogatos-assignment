/**
 * Navigation Bar Component
 * menu at the top of the page 
 */

import { Button, Nav, Navbar } from "react-bootstrap";
import logo from "../../assets/images/logo.svg";
import search from "../../assets/images/search_button.svg";
import FooterResponsive from "../footer-responsive/FooterResponsive";
import "./styles.scss";

const NavigationBar = () => {
  return (
    <Navbar expand="lg">
      <Navbar.Brand href="#home">
        <img src={logo} alt="logo" />
      </Navbar.Brand>
      <div className="d-xs-flex d-lg-none ml-auto">
        <Button className="btn btn-dark">
          <img className="d-block" src={search} alt="" />
        </Button>
      </div>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav
          defaultActiveKey="#home"
          className="mr-auto w-100 d-flex justify-content-center"
        >
          <Nav.Link href="#home" bsPrefix="navLink">
            Main One
          </Nav.Link>
          <Nav.Link href="#two" bsPrefix="navLink">
            Page Two
          </Nav.Link>
          <Nav.Link href="#three" bsPrefix="navLink">
            Page Three
          </Nav.Link>
          <Nav.Link href="#about" bsPrefix="navLink">
            About Us
          </Nav.Link>
          <Nav.Link href="#ourwork" bsPrefix="navLink">
            Our Work
          </Nav.Link>
          <Nav.Link href="#contact" bsPrefix="navLink">
            Contact
          </Nav.Link>
        </Nav>
        <div className="align-self-end d-flex align-items-center justify-content-center">
          <Button className="btn btn-dark language">EN</Button>
          <div className="verticalLine" />
          <Button className="btn btn-dark language">GR</Button>
          <Button className="btn btn-dark ml-4 display-sm-none">
            <img
              className="d-block"
              height={24}
              width={24}
              src={search}
              alt=""
            />
          </Button>
        </div>
        <FooterResponsive />
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavigationBar;
