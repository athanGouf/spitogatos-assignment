/**
 * Custom reusable button component
 * props
 * className: css classes
 * title: description of the button
 * default props of react bootstrap button
 */

import { Button } from "react-bootstrap";
import "./styles.scss";

const ButtonPrimary = (props) => (
  <Button {...props} className={`button-primary ${props.className}`}>
    {props.title}
  </Button>
);

export default ButtonPrimary;
