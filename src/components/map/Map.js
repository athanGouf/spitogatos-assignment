import { Col } from "react-bootstrap";
import "./styles.scss";
import map from "../../assets/images/map.png";

const Map = () => {
  return (
    <Col lg={5} className="pr-0">
      <img className="d-block w-100 img-responsive h-100" src={map} alt="Map" />
      <a className="company-location">
      </a>
    </Col>
  );
};

export default Map;
