import { Carousel } from "react-bootstrap";
import banner from "../../assets/images/banner.png";
import bannerMobile from "../../assets/images/banner_mobile.png";
import "./styles.scss";

const CarouselComp = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-lg-block d-none w-100"
          height={660}
          src={banner}
          alt="First slide"
        />
        <img
          className="d-sm-block d-lg-none banner-height"
          src={bannerMobile}
          alt="First responsive slide"
        />
        <Carousel.Caption className="carousel-content">
          <h1>S.und Co</h1>
          <div className="horizontalLine" />
          <p className="col-lg-6 col-md-12">
            To take a trivial example, which of us ever undertakes laborious
            physical exercise, except to obtain some advantage from it?
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
};

export default CarouselComp;
