/**
 * Footer of responsive menu
 */

import facebook from "../../assets/images/facebook.svg";
import twitter from "../../assets/images/twitter.svg";
import instagram from "../../assets/images/instagram.svg";
import social_media from "../../assets/images/social_media.svg";
import "./styles.scss";

const FooterResponsive = () => {
  return (
    <div className="d-xs-flex flex-fill d-lg-none background-footer pt-4 pb-2">
      <div className="d-flex justify-content-center">
        <img className="mr-4" src={facebook} alt="" />
        <img className="mr-4" src={twitter} alt="" />
        <img className="mr-4" src={instagram} alt="" />
        <img src={social_media} alt="" />
      </div>
      <div className="d-flex flex-row align-items-center justify-content-center py-4">
        <div className="language bg-transparent text-white mr-2">Cookies.</div>
        <div className="verticalLine" />
        <div className="language bg-transparent text-white ml-2">Privacy.</div>
      </div>
      <hr className="horizontal-line" />
      <div className="pl-4 text-white">
        <p className="footer-text mb-0">S.und@themail.com</p>
        <p className="mb-4 footer-text">+30 210 1234 567</p>
        <p className="footer-copyright mb-0">© The standard Copywrite 2020</p>
      </div>
    </div>
  );
};

export default FooterResponsive;
