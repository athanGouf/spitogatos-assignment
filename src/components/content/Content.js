/**
 * Content Component
 * the main content at the middle of the page
 */

import { Row, Col } from "react-bootstrap";
import popArt from "../../assets/images/pop_art.png";
import popArt2 from "../../assets/images/pop_art2.png";
import "./styles.scss";
import ButtonPrimary from "../button-primary/ButtonPrimary";

const Content = () => {
  return (
    <Row className="py-5 px-5 ml-0 mr-0">
      <Row>
        <Col md={12} lg={6}>
          <img className="d-block w-100" src={popArt} alt="pop art 1" />
        </Col>
        <Col md={12} lg={6} className="my-5">
          <h6 className="subTitle">Since the 1500s</h6>
          <h2 className="title">The standard</h2>
          <p className="w-75 description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt.
          </p>
        </Col>
      </Row>
      <Row className="py-5 row-responsive-flex">
        <Col
          md={12}
          lg={6}
          className="my-5 d-flex flex-column justify-content-center align-items-end"
        >
          <div className="col-md-12 col-lg-9">
            <h6 className="subTitle">Since the 1500s</h6>
            <h2 className="title">The standard</h2>
            <p className="w-75 description">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <ButtonPrimary title="Search" />
          </div>
        </Col>
        <Col md={12} lg={6} className="d-flex justify-content-center">
          <img className="d-block w-75" src={popArt2} alt="pop art 2" />
        </Col>
      </Row>
    </Row>
  );
};

export default Content;
