import { Row } from "react-bootstrap";
import Map from "../map/Map";
import ContactForm from "../contact-form/ContactForm";

const ContactContainer = () => {
  return (
    <Row className="row-responsive-flex mr-0">
      <Map />
      <ContactForm />
    </Row>
  );
};

export default ContactContainer;
