/**
 * Component for contact form
 *
 * Contains categories and subcategories request
 * Validation
 * 1. Fullname only latin characters
 * 2. Email only valid email
 * 3. Phone only ten digits
 * 4. Message max characters 100
 * 5. One checkbox must be selected
 */

import { useState, useEffect, useReducer } from "react";
import { Form, Row, Col, FormControl } from "react-bootstrap";
import "./styles.scss";
import categoriesAPI from "../../api/categories";
import ButtonPrimary from "../button-primary/ButtonPrimary";
import { validate } from "../../helpers/helpers";
import { initialState, reducer } from "../../helpers/reducer";

const ContactForm = () => {
  const [validated, setValidated] = useState(false);
  const [categoryPicked, setCategoryPicked] = useState("Category");
  const [categories, setCategories] = useState([]);
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    categoriesAPI((data) => {
      data[0].subCategories = data
        .map((i) => i.subCategories)
        .filter((i) => i)
        .flat();
      setCategories(data);
    });
  }, []);

  const handleSubmit = (event) => {
    const form = event.currentTarget;

    if (!validate(form, dispatch)) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  const onChangeCategory = ({ target }) => {
    setCategoryPicked(
      categories.filter((item) => item.categoryId == target.value)[0]
    );
  };

  return (
    <Col lg={7} className="form-container">
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <h2>Contact Us</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <Form.Group controlId="formFullName" className="my-4 position-relative">
          <Form.Control
            type="text"
            placeholder="Full Name *"
            custom
            className="textinput"
            isInvalid={validated && state.invalidName}
            required
          />
          <Form.Label className="textinput-label">Full Name *</Form.Label>
          <FormControl.Feedback type="invalid">Error Name</FormControl.Feedback>
        </Form.Group>

        <Form.Group controlId="formEmail" className="my-4 position-relative">
          <Form.Control
            custom
            type="email"
            placeholder="E-mail *"
            className="textinput"
            isInvalid={validated && state.invalidEmail}
            required
          />
          <Form.Label className="textinput-label">Email *</Form.Label>
          <FormControl.Feedback type="invalid">
            Error Email
          </FormControl.Feedback>
        </Form.Group>

        <Form.Group controlId="formPhone" className="my-4 position-relative">
          <Form.Control
            custom
            type="tel"
            placeholder="Phone *"
            className="textinput"
            isInvalid={validated && state.invalidPhone}
            required
          />
          <Form.Label className="textinput-label">Phone *</Form.Label>
          <FormControl.Feedback type="invalid">
            Error Phone
          </FormControl.Feedback>
        </Form.Group>

        <Row>
          <Col sm={12} lg={6}>
            <Form.Group controlId="categoriesSelect">
              <Form.Control
                custom
                className="textinput"
                as="select"
                onChange={onChangeCategory}
              >
                <option selected disabled hidden>
                  Category
                </option>
                {categories.map((item) => (
                  <option key={item.categoryId} value={item.categoryId}>
                    {item.name}
                  </option>
                ))}
              </Form.Control>
              <Form.Label className="select-label">Category</Form.Label>
            </Form.Group>
          </Col>
          <Col sm={12} lg={6} className="m-xs">
            <Form.Group controlId="subCategoriesSelect">
              <Form.Control custom className="textinput" as="select">
                <option selected disabled hidden>
                  SubCategory
                </option>
                {(categoryPicked?.subCategories || []).map((item) => (
                  <option key={item.subCategoryId}>{item.name}</option>
                ))}
              </Form.Control>
              <Form.Label className="select-label">SubCategory</Form.Label>
            </Form.Group>
          </Col>
        </Row>
        <Form.Group controlId="formMessage" className="my-4 position-relative">
          <Form.Control
            custom
            as="textarea"
            placeholder="Message"
            className="textinput"
            maxLength="100"
          />
          <Form.Label className="textinput-label">Message</Form.Label>
        </Form.Group>

        <Form.Label className="checkboxes-description">
          Please select at least one of the following:
        </Form.Label>
        <Form.Group
          custom
          controlId="checkboxGroup"
          className="d-flex flex-row"
        >
          <Form.Check
            custom
            id="check1"
            inline
            className="checkbox-container"
            type="checkbox"
            label="Option 1"
          />
          <Form.Check
            custom
            id="check2"
            inline
            className="ml-5 checkbox-container"
            type="checkbox"
            label="Option 2"
          />
        </Form.Group>
        <div className="checkbox-message">
          {state.invalidCheckbox ? "At least one option must be checked" : ""}
        </div>
        <div className="button-container">
          <ButtonPrimary
            type="submit"
            title="Submit"
            className="button-submit"
          />
        </div>
      </Form>
    </Col>
  );
};

export default ContactForm;
