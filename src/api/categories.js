import { URL } from "./config";

const categoriesAPI = (callback) => {
  fetch(URL, {
    method: "GET",
  }).then((response) => {
    response
      .json()
      .then((data) => {
        callback(data);
      })
      .catch((e) => {
        console.log(e);
      });
  });
};

export default categoriesAPI;
