export const initialState = {
  invalidName: false,
  invalidEmail: false,
  invalidPhone: false,
  invalidCheckbox: false,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case "set_invalid_name":
      return {
        ...state,
        invalidName: action.payload,
      };
    case "set_invalid_email":
      return {
        ...state,
        invalidEmail: action.payload,
      };
    case "set_invalid_phone":
      return {
        ...state,
        invalidPhone: action.payload,
      };
    case "set_invalid_checkbox":
      return {
        ...state,
        invalidCheckbox: action.payload,
      };
    default:
      throw new Error();
  }
};
