export const validateFullName = (fullname) => {
  return /^[A-Za-z\s]+$/.test(fullname);
};

export const validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
};

export const validatePhone = (phone) => {
  return /^\d{10}$/.test(phone);
};

export const validate = (form, dispatch) => {
  const { formFullName, formEmail, formPhone, check1, check2 } = form;

  let nameValidate = validateFullName(formFullName.value);
  let emailValidate = validateEmail(formEmail.value);
  let phoneValidate = validatePhone(formPhone.value);
  let checkboxValidate = check1.checked || check2.checked;

  dispatch({ type: "set_invalid_name", payload: !nameValidate });
  dispatch({ type: "set_invalid_email", payload: !emailValidate });
  dispatch({ type: "set_invalid_phone", payload: !phoneValidate });
  dispatch({ type: "set_invalid_checkbox", payload: !checkboxValidate });

  return nameValidate && emailValidate && phoneValidate && checkboxValidate;
};
